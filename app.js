import {users} from "./users.mjs"

let toulouseMap = [43.6043, 1.4437], myMap

function MapInitialize() {
    //initialize a Toulouse map
    myMap = L.map('mapId').setView(toulouseMap, 12) //latitude, longitude and zoom level

    // create our card with an external tile layer
    let tuileUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'

    //initialize our card with tileLayer function and openstreetmap
    let attrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
    L.tileLayer(tuileUrl, { minZoom: 8, maxZoom: 18, attribution: attrib }).addTo(myMap)
}

MapInitialize()

//use users array to display markers and group them with markerCluster
let markers = new L.MarkerClusterGroup({
    iconCreateFunction: function(cluster) {
        return L.divIcon({ 
            html: cluster.getChildCount(), 
            className: 'mycluster', 
            iconSize: null
        });
    }
})
users.map(user => markers.addLayer(L.marker([user.lat, user.long])))
myMap.addLayer(markers)



// add a marker in the given location
// let img = L.marker([43.6, 1.44]).addTo(myMap)

//on marker mouseover event
//img.addEventListener('mouseover', (e) => {
    //create an information popup
    //L.popup()
    //.setLatLng(e.latlng)
    //.setContent(`<div style="font-weight:bold; text-align:center">
    //    <img src="/home/dorine/Pictures/raph.jpg" width="40%"/> Raph</div>`
    //    .toString())
    //.openOn(myMap)
//})


//use users array to display markers
//users.map(user => L.marker([user.lat, user.long]).addTo(myMap))